
let querystring = require('querystring');
let http = require('http');
let express = require('express');
let bodyParser = require('body-parser');
let path = require("path");


/****************************
 * Lib CURL
 ***************************/

let Curl = {
  
	PROTOCOL_OPT: 'PROTOCOL',
	HOSTNAME_OPT: 'HOSTNAME',
	PATH_OPT: 'PATH',
	METHOD_OPT: 'METHOD',
	HEADERS_OPT: 'HEADERS',

	statusCode: 200, // Code de la reponse
	body: {},        // Corps de la reponse 
	data: {},        // Donnees en POST
	options: {},     // Options de la requete
	headers: {},     // Headers de la reponse


    create: function (options, data = {}) {
      let o = Object.create(Curl);
      
      o.init();
      Object.assign(o.options, options);
      Object.assign(o.data, data);

      return o;
    },
  
    init: function () {
  
      this.options = {
        protocole: 'http',
        hostname: '127.0.0.1',
        path: '/',
        method: 'GET',
        headers: {
          'User-Agent': 'CURL/1.0.0',
          'Content-Type': 'text/json',
          'X-MailerLite-ApiKey': ''
        }
      };
    },

    setOpt: function (optionName, optionValue) {
      if (optionName === Curl.HOSTNAME_OPT) this.options['hostname'] = optionValue;
      if (optionName === Curl.PATH_OPT) this.options['path'] = optionValue;
      if (optionName === Curl.METHOD_OPT) this.options['method'] = optionValue;
      if (optionName === Curl.HEADERS_OPT) {
        let a = optionValue.split(':');
        if( a.length >= 2 ) this.options['headers'][a[0].trim()] = a[1].trim();
      }
    },
  
    initReq: function (resolve, reject) {

		let _this = this;

		let req = http.request(_this.options, (res) => {

		_this.statusCode = res.statusCode;
		_this.headers = JSON.stringify(res.headers);

		res.setEncoding('utf8');

		res.on('data', (chunk) => {
		  _this.body += chunk;
		});

		res.on('end', () => {
		  //console.log('No more data in response.'); //---console
		  resolve(_this.body);
		});

		});   
		 
		req.on('error', (e) => {
		  //console.error(`problem with request: ${e.message}`); //---console
		  reject(e);
		});

		return req;
    },
  
    reqGET: function () {

      let _this = this;

      return new Promise(function (resolve, reject) {
  
        const req = _this.initReq(resolve, reject);  
        req.end();
      
      });
  
    },
  
    reqPOST: function (pData) {

      let _this = this;
  
      return new Promise(function (resolve, reject) {
        
        //const postData = querystring.stringify(_this.data);
		const postData = JSON.stringify(pData);
  
        const req = _this.initReq(resolve, reject);
    
        req.write(postData);
        req.end();
  
      });
    },
	
	reqHTTP: function(method, callback){
		
		this.options.method = method;
		
		if( method === 'GET' || method === 'DELETE' ){			
		  this.reqGET().then(function (body) {
			callback(body);
		  }, function (e) { 
			console.log(e); //---console
		  });
		}		
		
		if( method === 'POST' || method === 'PUT' ){			
		  this.reqPOST(this.data).then(function (body) {
			callback(body);
		  }, function (e) { 
			console.log(e); //---console
		  });
		}
	},
  
    get: function (callback) {
      this.reqHTTP('GET', callback);
    },
	
	del: function (callback) {
      this.reqHTTP('DELETE', callback);
    },
  
    post: function (callback) {
      this.reqHTTP('POST', callback);
    },
	
	put: function (callback) {
      this.reqHTTP('PUT', callback);
    },
  
  };
  

/****************************
 * Middleware MailerLite
 ***************************/

let MailerLite = {

  hostname: 'api.mailerlite.com',
  path: '/api/v2',
  apiKey: '',
  curl: undefined,

  init: function (apikey) {

    this.apiKey = apikey;
    
    this.curl = Curl.create({
      hostname: this.hostname,
      path: this.path,
      headers: { 'Content-Type': 'application/json', 'X-MailerLite-ApiKey': this.apiKey }
    });
    
    //console.log(this.apiKey); //---console
  },

  get: function (uri, callback) {
    this.curl.setOpt(Curl.PATH_OPT, this.path + uri);
    this.curl.get(callback);
  },
  
  del: function (uri, callback) {
    this.curl.setOpt(Curl.PATH_OPT, this.path + uri);
    this.curl.del(callback);
  },

  post: function (uri, reqBody, callback) { 
    this.curl.setOpt(Curl.PATH_OPT, this.path + uri);
	this.curl.data = reqBody;
    this.curl.post(callback);
  }
}


/****************************
 * Express JS
 ***************************/

//*
let app = express();
app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) { 
  
	res.sendFile('index.html');
  
});

// Authentifier un compte
/****************************/
app.post('/api/account', function (req, res) {
  
	let apiKey = req.body.apiKey;
	let uri = '/me';

	MailerLite.init(apiKey);
	MailerLite.get(uri, function (apiBody) {
		res.status = 200;
		res.send(apiBody);    
	});

});

// La liste des groupes
/****************************/
app.post('/api/groups', function (req, res) {
  
	let apiKey = req.body.apiKey;
	let uri = '/groups';

	MailerLite.init(apiKey);
	MailerLite.get(uri, function (apiBody) {
		res.status = 200;
		res.send(apiBody);    
	});  
});

// La liste des souscriptions
/****************************/
app.post('/api/contacts', function (req, res) { 

	let apiKey = req.body.apiKey;
	let groupId = req.body.groupId;
	let uri = '/groups/'+groupId+'/subscribers';

	MailerLite.init(apiKey);
	MailerLite.get(uri, function (apiBody) {
		res.status = 200;
		res.send(apiBody);    
	});

});

// Ajouter une souscription
/****************************/
app.post('/api/contact/add', function (req, res) { 

	let apiKey = req.body.apiKey;
	let groupId = req.body.groupId;
	let contactEmail = req.body.contactEmail;
	let uri = '/groups/'+groupId+'/subscribers';
	let data = { email: contactEmail };

	MailerLite.init(apiKey);
	MailerLite.post(uri, data, function (apiBody) {
		res.status = 200;
		res.send(apiBody);    
	});

});

// Supprimer une souscription
/****************************/
app.post('/api/contact/delete', function (req, res) { 

	let apiKey = req.body.apiKey;
	let groupId = req.body.groupId;
	let contactEmail = req.body.contactEmail;
	let uri = '/groups/'+groupId+'/subscribers/'+contactEmail;
	
	MailerLite.init(apiKey);
	MailerLite.del(uri, function (apiBody) {
		res.status = 200;
		res.send(apiBody);
	});

});


app.listen(8000, function () {
	console.log('Application demarre sur le port 8000...\nOuvrir le navigateur sur http://localhost:8000');
});
//*/
