
let querystring = require('querystring');
let http = require('http');
let express = require('express');
let bodyParser = require('body-parser');
let path = require("path");
const request = require('superagent');
const fs = require('fs');
const Url = require('url');



/****************************
 * Express JS
 ***************************/

//*
let app = express();
app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) { 
  
	res.sendFile('index.html');
  
});

app.get('/crawler', function (req, res) { 
  
	res.sendFile('./crawler.html', {root: path.join(__dirname)});
	//res.sendFile('index.html');
  
});

// Crawler
/****************************/
app.post('/api/crawler', function (req, res) {
  
	var urls = req.body.urls;
			
	if (!urls) {
		console.log('bad urls 1', urls);
		return res.status(400).send('Bad Urls');
	}
	urls = urls.split(',');
	if (!urls || !urls.length) {
		console.log('bad urls', urls);
		return res.status(400).send('Bad Urls');
	}
	
	var done = urls.length, error = false, data = [];
	for (var i=0; i<urls.length; ++i) {
		(function (i) {
			if (error) return;
			var url = urls[i];
			var myUrl = Url.parse(url);
			if (!url || !myUrl) {
				console.log('bad url', url);
				error = true;
				return res.status(400).send('Bad Url');
			}
			if (myUrl.host != 'www.amazon.fr') {
				console.log('not www.amazon.fr url', url);
				error = true;
				return res.status(400).send('Not www.amazon.fr url');
			}
			request
				.get(url)
				//.query({ action: 'edit', city: 'London' }) // query string
				.end((err, rep) => {
					if (err || !rep) {
						console.log('error on ', url, err);
						error = true;
						return res.status(400).send('Not found');
					}
					fs.writeFile('amazon.html', rep.text, (err) => {
						console.log(myUrl.host+myUrl.pathname, 'fetched');
						data[i] = rep.text;
						if (--done) return;
						res.status(200).json(data);
					});
				// Do something
			});
		})(i);
	}

});



app.listen(8000, function () {
	console.log('Application demarre sur le port 8000...\nOuvrir le navigateur sur http://localhost:8000');
});
//*/
