(function(){
	
	var async_loop = function (fundamental){

		var loop = function (i, length, after){
			if (i >= length) {
				if (!after) return;
				if (after.args && after.func) 
				after.func.apply(after.this, after.args);
				return;
			}
			fundamental(loop, i, length, after );
		};

		return loop;
	};


	
	var crawlerApp = angular.module('crawlerApp', ['summernote', 'ngclipboard']);
	
	crawlerApp.controller('ProductController', ['$scope', '$http', '$rootScope', ProductController]);
	
	function ProductController ($scope, $http, $rootScope) {
		
		var self = this;
		window.S = self;
		
		self = $scope;
		self.fetchProducts = fetchProducts;
		self.saveProducts = saveProducts;
		self.resetWindow = resetWindow;
		self.scroll = scroll;
		
		self.status = {};
		self.products = [];
		self.defaultp = {
			title: '', imgs: [], vendor: 'Libraire des Peuples Noirs', type: 'Book', sku: '100', collection: 'bloc 13'
		};
		self.amazonUrls = 'https://www.amazon.fr/LAvenir-vie-Edward-Wilson/dp/2020549425/ref=sr_1_1?s=books&ie=UTF8&qid=1519640339&sr=1-1&keywords=l%27avenir+de+la+vie+wilson,https://www.amazon.fr/conqu%C3%AAte-sociale-terre-Edward-Wilson/dp/2081290146/ref=pd_sbs_14_1?_encoding=UTF8&psc=1&refRID=J5X42ZCSEYSHZ7VPT4HK';
		
		
		function scroll(direction) {
			if (direction < 0) $("html, body").animate({scrollTop: $(document).height()    }, 100);                  
			else $("html, body").animate({scrollTop: 0    }, 100);                  

		}
		
		function resetWindow() {
			history.go(0);
		}
		function fetchProducts () {
			var urls = self.amazonUrls ? self.amazonUrls.split(',') : [], product;
			if (!urls || !urls.length) return;
			self.status.fetching = true;
			self.status.error = '';
			self.status.workingd = '';
			angular.copy([], self.products);
			for (var i=0; i<urls.length; ++i) {
				self.products[i] = angular.copy(self.defaultp);
			}
			
			var data = {urls: self.amazonUrls};
			var done = {args:[], func: function () {self.status.fetching = false;self.status.error = '';self.status.workingd = 'Done'}};
			self.status.workingd = 'Fetching Products from Amazon ...';
			$http.post('/api/crawler', data).then(
				function (rep) {
					console.log(rep.data.length, rep.data);
					var length = rep.data.length;
					var repeater =  async_loop(function (loop, i, length, after) {
						self.status.workingd = 'Parsing product '+i+'...';
						success(rep.data[i], self.products[i], function () {
							$rootScope.$apply(function () {
								self.status.current = i;
								loop(i+1, length, after);
							});
						});
					});
					repeater(0, length, done);
				},
				function (err) {
					self.status.fetching = false;
					self.status.error = err.responseText;
					console.error(err);	
				}
			)
		}
		
		function getHandler (str) {
			if (!str) return '';
			return str
				  .toLowerCase()
				  .replace(/['"`]/g, '')
				  .replace(/[àâ]/g, 'a')
				  .replace(/ç/g, 'c')
				  .replace(/[éèê]/g, 'e')
				  .replace(/î/g, 'i')
				  .replace(/ô/g, 'o')
				  .replace(/[ùû]/g, 'u')
				  .replace(/ [^a-zA-Z0-9]/g, ' ')
				  .replace(/[^a-zA-Z0-9] /g, ' ')
				  .replace(/[^a-zA-Z0-9]/g, '-');
		}
		
		function duplicateQuote(str) {
			if (!str) return '';
			return str.replace(/"/g, '""');
		}
		
		function convertPrice(price) {
			if (!price) return 0;
			price = price.replace(/,/g, '.');
			if (isNaN(price)) return 0;
			return Math.ceil(price);
		}
		
		function saveProducts () {
			var csv = 'Handle,Title,Body (HTML),Vendor,Type,Collection,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,Image Position,Image Alt Text,Gift Card,SEO Title,SEO Description,Google Shopping / Google Product Category,Google Shopping / Gender,Google Shopping / Age Group,Google Shopping / MPN,Google Shopping / AdWords Grouping,Google Shopping / AdWords Labels,Google Shopping / Condition,Google Shopping / Custom Product,Google Shopping / Custom Label 0,Google Shopping / Custom Label 1,Google Shopping / Custom Label 2,Google Shopping / Custom Label 3,Google Shopping / Custom Label 4,Variant Image,Variant Weight Unit,Variant Tax Code\n';
				
			for (var k=0; k<self.products.length; ++k) {
				var product = self.products[k];
				if (product.disabled) continue; // we don't take care of disabled products
				var desc = product.desc || product.sdesc || '';
				desc += (product.detail || '');
				var i = -1, j, handler = getHandler(product.title);
				
				csv += handler; ++i
				csv += ',"'+duplicateQuote(product.title)+'"'; ++i;
				csv += ',"'+duplicateQuote(desc)+'"'; ++i;
				csv += ',"'+(product.vendor||'')+'"'; ++i;
				csv += ',"'+(product.type||'')+'"'; ++i;
				csv += ',"'+duplicateQuote(product.collection)+'"'; ++i;
				csv += ',""';  ++i; // no tags 
				csv += ',true';  ++i;// published
				//"Libraire des Peuples Noirs,Book,"",true,Title,Default Title,,,,,"",0,shopify,5,deny,manual,3000,3,true,false,"",https://cdn.shopify.com/s/files/1/2792/3132/products/book-136.jpg?v=1517084199,1,,false,,,,,,,,,,,,,,,,,kg,"
				csv += ',Title';  ++i;
				csv += ',Default Title';  ++i;
				csv += ',,,,,""';  i+= 5; // OptionI Value|Name
				csv += ','+product.sku; ++i; 
				csv += ',shopify,5,deny,manual'; i += 4;
				csv += ','+(product.lprice||''); ++i;
				csv += ','+convertPrice(product.price); ++i;
				csv += ',true'; ++i; // shipping
				csv += ',false'; ++i; // taxable
				csv += ',""'; ++i;
				csv += ','+product.imgs[0]; j = ++i; 
				//'https://cdn.shopify.com/s/files/1/2792/3132/products/book-136.jpg?v=1517084199,1,,false,,,,,,,,,,,,,,,,,kg,'
				csv += ',1,,false'; i+= 3; //img pos, img alt, gift card
				csv += ',,,,,,,,,,,,,,,,,kg,'; i+= 18;
				csv += '\n';
				for (var i=1; i<product.imgs.length; ++i) {
					csv += handler+',,,,,,,,,,,,,,,,,,,,,,,,'+ ',' +/*for Collection header*/product.imgs[i]+','+(i+1)+',,,,,,,,,,,,,,,,,,,,\n';
				}
			}	
			self.status.csv = csv;
		}
	
	
		function parseDetail($detail, $author) {
			var detail = '<p></p>', author = '<li>Auteur: ';
			var temp, length = $author.find('.a-size-medium').length;
			if (length) {
				$author.find('.a-size-medium').each(function (index) {
					temp = $(this).text();
					if (!temp) return;
					temp = temp.split('(');
					if (!temp || !temp[0]) return;
					temp = temp[0].trim();
					author += temp;
					if (index < length - 1) author += ', ';
				});
			}
			length = $author.find('.author').length;
			if (!temp && length) {
				$author.find('.author').each(function (index) {
					temp = $(this).text();
					if (!temp) return;
					temp = temp.split('(');
					if (!temp || !temp[0]) return;
					temp = temp[0].trim();
					author += temp;
					if (index < length - 1) author += ', ';
				});
			}
			
			if (!temp) author += '</li>';
			
			var done = {page: false, editor: false, collection: false};
			$detail.find('li').each(function (index) { //get pages
				if (done.page) return;
				temp = $(this).text() || ''; 
				if (temp.indexOf('page') == -1 && temp.indexOf('Page') == -1) return;
							
				temp = temp.split(':');
				if (!temp || !temp[1]) return;
				temp = temp[1];
				temp = temp.split('pages');
				if (!temp || !temp[0]) return;
				temp = temp[0].trim();
				detail += '<li>Pages: '+temp+' </li>';
				done.page = true;
				return;
			});
			if (!done.page) detail += '<li>Pages:  </li>';
			detail += '<li>Langue: Français </li>';
			detail += author;
				
			$detail.find('li').each(function (index) { //get editor
				if (done.editor) return;
				temp = $(this).text() || '' 
				
				if (temp.indexOf('editeur') == -1 && temp.indexOf('Editeur') == -1) return;
				temp = temp.split(':');
				if (!temp || !temp[1]) return;
				temp = temp[1].trim();
				detail += '<li>Editeur: '+temp+' </li>';
				done.editor = true;
				return;
			});
			if (!done.editor) detail += '<li>Editeur:  </li>';
				
			$detail.find('li').each(function (index) { //get editor
				if (done.collection) return;
				temp = $(this).text() || '' 
				
				if (temp.indexOf('collection') == -1 && temp.indexOf('Collection') == -1) return;
				temp = temp.split(':');
				if (!temp || !temp[1]) return;
				temp = temp[1].trim();
				detail += '<li>Collection: '+temp+' </li>';
				done.collection = true;
				return;
			});
			if (!done.collection) detail += '<li>Collection:  </li>';
				
			console.warn(detail);
			return detail;
		}
			
			
		function success (data, product, done) {
				
			var $data = $('<div><div>'+data+'</div></div>');
			$('#amazon').html($data.html());
				
			setTimeout(function () {
				var title = $('#productTitle').text() || $('#ebooksProductTitle').text();
				var sdesc = '<h3 class="productDescriptionSource">Présentation de l\'Editeur</h3>' + 
								$('#bookDesc_iframe').contents().find('#iframeContent').html(); 
				var desc = $('#product-description-iframe').contents().find('#productDescription .content').html();
				var detail = $('#detail_bullets_id .content').html();
				var author = $('#byline').html();
				var price = $('#tmmSwatches .selected .a-color-price').text() || ''; 
				price = price.split('EUR');
				if (!price[1]) price = null; else price = price[1].trim();
				var imgs = []; 
				$('#imageBlockThumbs .imageThumb.thumb img').each(function (){ imgs.push($(this).attr('src'))});
				if (!imgs.length && $('#ebooks-img-canvas img').attr('src')) imgs.push($('#ebooks-img-canvas img').attr('src'));
					
				product.title = title;
				product.price = price;
				product.sdesc = sdesc;
				product.desc = desc || sdesc;
				product.detail = parseDetail($('<div>'+detail+'</div>'), $('<div>'+author+'</div>'));
				product.imgs = [];
				var img, beforeLast;
				for (var i=0; i<imgs.length; ++i) {
					img = imgs[i].split('.');
					beforeLast = img[img.length-2];
					if (beforeLast[0] != '_') { product.imgs.push(imgs[i]); continue;} //does not start with _
					img.splice( img.length-2, 1); //star with _, we remove that element to get the real image link
					product.imgs.push(img.join('.'));
				}
				$('#amazon').html('');
				done();
				
			}, 5000);
		}
		
	}
})();
	  
	  